
describe('initMap()', function () {
     it('tunnistaa, tukeeko selain paikannusta', function() {
        if(browserSupportFlag===true) {
            expect(browserSupportFlag).toBe(true);
        } else {
            expect(browserSupportFlag).toBe(false);
        }
    });
    
});

describe('removeLines()', function() {
    it('poistaa näkyvillä olevat reitit kartalta', function() {
        removeLines();
        expect(Paths.length).toBe(0);
    }); 
        
});

describe('removeStopMarkers()', function() {
    it('poistaa näkyvillä olevat pysäkit kartalta', function() {
        removeStopMarkers();
        expect(markers.length).toBe(0);
    });
});

describe('removeTramMarkers()', function() {
    it('poistaa näkyvillä olevat ratikat kartalta', function() {
        removeTramMarkers();
        expect(tramMarkers.length).toBe(0);
    });
});

describe('drawLines(line, direction)', function() {
    it('hakee restAPIsta reitin tiedot', function() {
        
        expect(drawLines(1001,1)).toBe(false);
    });
});

describe('drawStops(line, direction)', function() {
    it('hakee restAPIsta pysäkkien tiedot', function() {
        drawStops(1001,1);
        expect(markers.length > 0).toBe(false);
    });
});

describe('drawTrams(line, direction)', function() {
    it('hakee restAPIsta ratikoiden tiedot', function() {
        drawTrams(1001,1);
        expect(tramMarkers.length > 0).toBe(false);
    });
});
