<?php

/**
 * An array named _lines_ is created to define id's for the tram lines. This is used in the for-loop below.
 */
 
$lines = ['1001', '1001A', '1002', '1003', '1004', '1004T', '1006', '1006T', '1007A', '1007B', '1008', '1009', '1010'];
/**
 * Connection to mongoDB is made here. The correct collection is also defined. The drop-command clears the whole collection
 * before adding the data again (if needed).
 */
$connection = new MongoClient();
$collection = $connection->traminfo->stops;
$collection->drop();

/**
 * List of information of all the tram stops available. Name, address and id of the stops separated by ?-symbol
 */ 
$stops['1001.1']= [
'Kauppatori?Eteläesplanadi?0429',
'Hallituskatu?Snellmaninkatu 3?0451',
'Kansallisarkisto?Snellmaninkatu 13?0453',
'Snellmaninkatu?Liisankatu 25?0455',
'Hakaniemi?Siltasaarenkatu?0251',
'Kallion virastotalo?Siltasaarenkatu 11?0253',
'Karhupuisto?Viides linja?0255',
'Kaarlenkatu?Kaarlenkatu 11?0257',
'Urheilutalo?Läntinen Brahenkatu?0259',
'Brahenkatu?Läntinen Brahenkatu?0261',
'Roineentie?Sturenkatu 23?0263',
'Hattulantie?Mäkelänkatu 31?0265',
'Rautalammintie?Mäkelänkatu 37?0267',
'Mäkelänrinne?Mäkelänkatu 45?0269',
'Uintikeskus?Mäkelänkatu?0271',
'Pyöräilystadion, Laituri 22?Mäkelänkatu?0273',
'Koskelantie?Mäkelänkatu?0275',
'Kimmontie?Mäkelänkatu 93?0279',
'Käpylänaukio?Mäkelänkatu 99?0281',
'Metsolantie?Pohjolankatu 25?0283',
'Pohjolanaukio?Pohjolanaukio?0285'];

$stops['1001.2']=[
'Pohjolanaukio?Pohjolanaukio?0285',
'Metsolantie?Pohjolankatu 18?0284',
'Käpylänaukio?Mäkelänkatu 98?0282',
'Kimmontie?Mäkelänkatu 84?0280',
'Koskelantie?Mäkelänkatu?0276',
'Pyöräilystadion, Laituri 21?Mäkelänkatu?0274',
'Uintikeskus?Mäkelänkatu?0272',
'Mäkelänrinne?Mäkelänkatu 60?0270',
'Rautalammintie?Mäkelänkatu 46?0268',
'Hattulantie?Mäkelänkatu 30?0266',
'Roineentie?Sturenkatu 18?0264',
'Brahenkatu?Läntinen Brahenkatu?0262',
'Urheilutalo?Läntinen Brahenkatu?0260',
'Kaarlenkatu?Kaarlenkatu?0258',
'Karhupuisto?Viides linja?0256',
'Kallion virastotalo?Porthaninkatu 2?0254',
'Hakaniemi?Siltasaarenkatu?0252',
'Snellmaninkatu?Liisankatu 20?0454',
'Kansallisarkisto?Snellmaninkatu 8?0452',
'Hallituskatu?Snellmaninkatu 4?0450',
'Senaatintori?Aleksanterinkatu?0448',
'Kauppatori?Eteläesplanadi?0429'];

$stops['1001A.1']=[
'Telakkakatu?Pursimiehenkatu 29?0446',
'Perämiehenkatu?Tehtaankatu 34?0445',
'Eiran sairaala?Tehtaankatu 30?0443',
'Kapteeninkatu?Tehtaankatu?0441',
'Neitsytpolku?Tehtaankatu 12?0439',
'Kaivopuisto?Tehtaankatu 2?0437',
'Olympialaituri?Laivasillankatu?0435',
'Eteläranta?Eteläranta?0433',
'Kauppatori?Eteläranta?0431',
'Hallituskatu?Snellmaninkatu 3?0451',
'Kansallisarkisto?Snellmaninkatu 13?0453',
'Snellmaninkatu?Liisankatu 25?0455',
'Hakaniemi?Siltasaarenkatu?0251',
'Kallion virastotalo?Siltasaarenkatu 11?0253',
'Karhupuisto?Viides linja?0255',
'Kaarlenkatu?Kaarlenkatu 11?0257',
'Urheilutalo?Läntinen Brahenkatu?0259',
'Brahenkatu?Läntinen Brahenkatu?0261',
'Roineentie?Sturenkatu 23?0263',
'Hattulantie?Mäkelänkatu 31?0265',
'Rautalammintie?Mäkelänkatu 37?0267',
'Mäkelänrinne?Mäkelänkatu 45?0269',
'Uintikeskus?Mäkelänkatu?0271',
'Pyöräilystadion, Laituri 22?Mäkelänkatu?0273',
'Koskelantie?Mäkelänkatu?0275',
'Kimmontie?Mäkelänkatu 93?0279',
'Käpylänaukio?Mäkelänkatu 99?0281',
'Metsolantie?Pohjolankatu 25?0283',
'Pohjolanaukio?Pohjolanaukio?0285'];

$stops['1001A.2']=[
'Pohjolanaukio?Pohjolanaukio?0285',
'Metsolantie?Pohjolankatu 18?0284',
'Käpylänaukio?Mäkelänkatu 98?0282',
'Kimmontie?Mäkelänkatu 84?0280',
'Koskelantie?Mäkelänkatu?0276',
'Pyöräilystadion, Laituri 21?Mäkelänkatu?0274',
'Uintikeskus?Mäkelänkatu?0272',
'Mäkelänrinne?Mäkelänkatu 60?0270',
'Rautalammintie?Mäkelänkatu 46?0268',
'Hattulantie?Mäkelänkatu 30?0266',
'Roineentie?Sturenkatu 18?0264',
'Brahenkatu?Läntinen Brahenkatu?0262',
'Urheilutalo?Läntinen Brahenkatu?0260',
'Kaarlenkatu?Kaarlenkatu?0258',
'Karhupuisto?Viides linja?0256',
'Kallion virastotalo?Porthaninkatu 2?0254',
'Hakaniemi?Siltasaarenkatu?0252',
'Snellmaninkatu?Liisankatu 20?0454',
'Kansallisarkisto?Snellmaninkatu 8?0452',
'Hallituskatu?Snellmaninkatu 4?0450',
'Senaatintori?Aleksanterinkatu?0448',
'Kauppatori?Eteläesplanadi?0430',
'Eteläranta?Eteläranta 10?0432',
'Olympialaituri?Laivasillankatu?0434',
'Kaivopuisto?Tehtaankatu?0436',
'Neitsytpolku?Tehtaankatu 1?0438',
'Kapteeninkatu?Tehtaankatu 11?0440',
'Eiran sairaala?Tehtaankatu 21?0442',
'Perämiehenkatu?Tehtaankatu?0444',
'Telakkakatu?Pursimiehenkatu 29?0446'];

$stops['1002.1']=[
'Olympialaituri?Laivasillankatu?0435',
'Eteläranta?Eteläranta?0433',
'Kauppatori?Eteläranta?0431',
'Senaatintori?Aleksanterinkatu?0406',
'Aleksanterinkatu?Aleksanterinkatu 13?0404',
'Mikonkatu?Mikonkatu?0425',
'Rautatieasema?Kaivokatu?0302',
'Simonkatu?Simonkatu?0231',
'Kamppi(M), Laituri 74?Fredrikinkatu?0233',
'Kauppakorkeakoulut?Arkadiankatu 20?0203',
'Sammonkatu?Runeberginkatu 31?0205',
'Apollonkatu?Runeberginkatu?0207',
'Töölöntori, Laituri 22?Runeberginkatu 55?0209',
'Ooppera?Mannerheimintie?0107',
'Töölön halli?Mannerheimintie 29?0109',
'Kansaneläkelaitos?Nordenskiöldinkatu 9?0641',
'Auroran sairaala?Nordenskiöldinkatu?0640',
'Eläintarha?Nordenskiöldinkatu?0638'];

$stops['1002.2']=[
'Eläintarha?Nordenskiöldinkatu?0637',
'Auroran sairaala?Nordenskiöldinkatu?0639',
'Kansaneläkelaitos?Mannerheimintie 94?0112',
'Töölön halli?Mannerheimintie 76?0110',
'Ooppera?Mannerheimintie 58?0108',
'Töölöntori, Laituri 21?Runeberginkatu 46?0210',
'Apollonkatu?Runeberginkatu 32?0208',
'Sammonkatu?Runeberginkatu?0206',
'Kauppakorkeakoulut?Arkadiankatu 23?0204',
'Kamppi (M), Laituri 73?Fredrikinkatu?0234',
'Simonkatu?Simonkatu?0232',
'Rautatieasema?Kaivokatu?0301',
'Mikonkatu?Mikonkatu?0424',
'Aleksanterinkatu?Aleksanterinkatu 42?0403',
'Senaatintori?Aleksanterinkatu?0405',
'Kauppatori?Eteläesplanadi?0430',
'Eteläranta?Eteläranta 10?0432',
'Olympialaituri?Laivasillankatu?0434'];

$stops['1003.1']=[
'Olympialaituri?Laivasillankatu?0434',
'Kaivopuisto?Tehtaankatu?0436',
'Neitsytpolku?Tehtaankatu 1?0438',
'Kapteeninkatu?Tehtaankatu 11?0440',
'Eiran sairaala?Tehtaankatu 21?0442',
'Viiskulma?Fredrikinkatu 19?0815',
'Iso Roobertinkatu?Fredrikinkatu 21?0813',
'Fredrikinkatu?Bulevardi 18-20?0803',
'Erottaja?Bulevardi 6?0801',
'Ylioppilastalo?Mannerheimintie 1?0701',
'Rautatieasema?Kaivokatu?0301',
'Kaisaniemenkatu?Kaisaniemenkatu 7?0303',
'Kaisaniemenpuisto?Kaisaniemenkatu?0305',
'Hakaniemi?Siltasaarenkatu?0251',
'Kallion virastotalo?Siltasaarenkatu 11?0253',
'Karhupuisto?Viides linja?0255',
'Kaarlenkatu?Kaarlenkatu 11?0257',
'Urheilutalo?Läntinen Brahenkatu?0259',
'Porvoonkatu?Porvoonkatu?0631',
'Linnanmäki?Viipurinkatu 13?0633',
'Karjalankatu?Viipurinkatu 31?0635',
'Eläintarha?Nordenskiöldinkatu?0637'];

$stops['1003.2']=[
'Eläintarha?Nordenskiöldinkatu?0638',
'Karjalankatu?Viipurinkatu?0636',
'Linnanmäki?Viipurinkatu 20?0634',
'Porvoonkatu?Porvoonkatu 12?0632',
'Urheilutalo?Läntinen Brahenkatu?0260',
'Kaarlenkatu?Kaarlenkatu?0258',
'Karhupuisto?Viides linja?0256',
'Kallion virastotalo?Porthaninkatu 2?0254',
'Hakaniemi?Siltasaarenkatu?0252',
'Kaisaniemenpuisto?Kaisaniemenkatu?0306',
'Kaisaniemenkatu?Kaisaniemenkatu?0304',
'Rautatieasema?Kaivokatu?0302',
'Ylioppilastalo?Mannerheimintie?0702',
'Erottaja?Bulevardi 3?0802',
'Fredrikinkatu?Bulevardi 9?0804',
'Iso Roobertinkatu?Fredrikinkatu 20?0814',
'Viiskulma?Fredrikinkatu 12?0816',
'Eiran sairaala?Tehtaankatu 30?0443',
'Kapteeninkatu?Tehtaankatu?0441',
'Neitsytpolku?Tehtaankatu 12?0439',
'Kaivopuisto?Tehtaankatu 2?0437',
'Olympialaituri?Laivasillankatu?0435'];

$stops['1004.1']=[
'Merisotilaantori?Merikasarminkatu?0421',
'Puolipäivänkatu?Merikasarminkatu?0416',
'Vyökatu?Merikasarminkatu?0414',
'Kauppiaankatu?Kruunuvuorenkatu 7?0412',
'Tove Janssonin p.?Satamakatu?0410',
'Ritarihuone?Aleksanterinkatu?0408',
'Senaatintori?Aleksanterinkatu?0406',
'Aleksanterinkatu?Aleksanterinkatu 13?0404',
'Ylioppilastalo?Aleksanterinkatu?0402',
'Lasipalatsi?Mannerheimintie 9?0101',
'Kansallismuseo?Mannerheimintie?0103',
'Hesperian puisto?Mannerheimintie?0105',
'Ooppera?Mannerheimintie?0107',
'Töölön halli?Mannerheimintie 29?0109',
'Kansaneläkelaitos?Mannerheimintie 47?0111',
'Töölön tulli?Tukholmankatu?0113',
'Meilahden sairaala, Laituri 26?Tukholmankatu 15?0115',
'Meilahdentie?Paciuksenkatu?0117',
'Paciuksenkaari?Paciuksenkatu?0119',
'Munkkin. puistotie?Munkkin. puistotie 7?0121',
'Laajalahden aukio?Munkkin. puistotie?0123',
'Tiilimäki?Laajalahdentie 4?0125',
'Saunalahdentie?Saunalahdentie?0127'];

$stops['1004.2']=[
'Saunalahdentie?Saunalahdentie?0127',
'Tiilimäki?Hollantilaisentie?0126',
'Laajalahden aukio?Munkkin. puistotie?0124',
'Munkkin. puistotie?Munkkin.puistotie 10?0122',
'Paciuksenkaari?Paciuksenkatu?0120',
'Meilahdentie?Paciuksenkatu?0118',
'Meilahden sairaala, Laituri 25?Tukholmankatu?0116',
'Töölön tulli?Tukholmankatu 2?0114',
'Kansaneläkelaitos?Mannerheimintie 94?0112',
'Töölön halli?Mannerheimintie 76?0110',
'Ooppera?Mannerheimintie 58?0108',
'Hesperian puisto?Mannerheimintie 42?0106',
'Kansallismuseo?Mannerheimintie?0104',
'Lasipalatsi?Mannerheimintie?0102',
'Ylioppilastalo?Aleksanterinkatu?0401',
'Aleksanterinkatu?Aleksanterinkatu 42?0403',
'Senaatintori?Aleksanterinkatu?0405',
'Ritarihuone?Aleksanterinkatu 10?0407',
'Tove Janssonin p.?Kruunuvuorenkatu?0409',
'Kauppiaankatu?Kruunuvuorenkatu?0411',
'Vyökatu?Merikasarminkatu?0413',
'Puolipäivänkatu?Merikasarminkatu?0415',
'Merisotilaantori?Merisotilaantori?0417'];

$stops['1004T.1']=[
'Katajanokan term.?Mastokatu?0420',
'Mastokatu?Kruunuvuorenkatu 15?0418',
'Kauppiaankatu?Kruunuvuorenkatu 7?0412',
'Tove Janssonin p.?Satamakatu?0410',
'Ritarihuone?Aleksanterinkatu?0408',
'Senaatintori?Aleksanterinkatu?0406',
'Aleksanterinkatu?Aleksanterinkatu 13?0404',
'Ylioppilastalo?Aleksanterinkatu?0402',
'Lasipalatsi?Mannerheimintie 9?0101',
'Kansallismuseo?Mannerheimintie?0103',
'Hesperian puisto?Mannerheimintie?0105',
'Ooppera?Mannerheimintie?0107',
'Töölön halli?Mannerheimintie 29?0109',
'Kansaneläkelaitos?Mannerheimintie 47?0111',
'Töölön tulli?Tukholmankatu?0113',
'Meilahden sairaala, Laituri 26?Tukholmankatu 15?0115',
'Meilahdentie?Paciuksenkatu?0117',
'Paciuksenkaari?Paciuksenkatu?0119',
'Munkkin. puistotie?Munkkin. puistotie 7?0121',
'Laajalahden aukio?Munkkin. puistotie?0123',
'Tiilimäki?Laajalahdentie 4?0125',
'Saunalahdentie?Saunalahdentie?0127'];

$stops['1004T.2']=[
'Saunalahdentie?Saunalahdentie?0127',
'Tiilimäki?Hollantilaisentie?0126',
'Laajalahden aukio?Munkkin. puistotie?0124',
'Munkkin. puistotie?Munkkin.puistotie 10?0122',
'Paciuksenkaari?Paciuksenkatu?0120',
'Meilahdentie?Paciuksenkatu?0118',
'Meilahden sairaala, Laituri 25?Tukholmankatu?0116',
'Töölön tulli?Tukholmankatu 2?0114',
'Kansaneläkelaitos?Mannerheimintie 94?0112',
'Töölön halli?Mannerheimintie 76?0110',
'Ooppera?Mannerheimintie 58?0108',
'Hesperian puisto?Mannerheimintie 42?0106',
'Kansallismuseo?Mannerheimintie?0104',
'Lasipalatsi?Mannerheimintie?0102',
'Ylioppilastalo?Aleksanterinkatu?0401',
'Aleksanterinkatu?Aleksanterinkatu 42?0403',
'Senaatintori?Aleksanterinkatu?0405',
'Ritarihuone?Aleksanterinkatu 10?0407',
'Tove Janssonin p.?Kruunuvuorenkatu?0409',
'Kauppiaankatu?Kruunuvuorenkatu?0411',
'Mastokatu?Kruunuvuorenkatu 6?0419',
'Katajanokan term.?Mastokatu?0420'];

$stops['1006.1']=[
'Hietalahti?Ruoholahdenranta?0811',
'Köydenpunojankatu?Hietalahdenranta 15?0809',
'Hietalahdentori?Bulevardi 42?0807',
'Aleksant. teatteri?Bulevardi 30?0805',
'Fredrikinkatu?Bulevardi 18-20?0803',
'Erottaja?Bulevardi 6?0801',
'Ylioppilastalo?Mannerheimintie 1?0701',
'Rautatieasema?Kaivokatu?0301',
'Kaisaniemenkatu?Kaisaniemenkatu 7?0303',
'Kaisaniemenpuisto?Kaisaniemenkatu?0305',
'Hakaniemi?Siltasaarenkatu?0307',
'Haapaniemi?Hämeentie?0309',
'Käenkuja?Hämeentie 15?0311',
'Sörnäinen(M)?Hämeentie 33?0313',
'Lautatarhankatu?Hämeentie?0315',
'Hauhon puisto?Hämeentie 63?0317',
'Vallilan varikko?Hämeentie 85?0319',
'Paavalin kirkko?Hämeentie?0321',
'Kyläsaarenkatu?Hämeentie?0323',
'Toukoniitty?Hämeentie?0325',
'Arabiankatu?Arabiankatu?0335',
'Arabianranta?Arabianranta?0327'];

$stops['1006.2']=[
'Arabianranta?Arabianranta?0327',
'Arabiankatu?Arabiankatu 2?0336',
'Toukoniitty?Hämeentie 122?0328',
'Kyläsaarenkatu?Hämeentie?0326',
'Paavalin kirkko?Hämeentie?0330',
'Vallilan varikko?Hämeentie 94?0320',
'Hauhon puisto?Hämeentie?0318',
'Lautatarhankatu?Hämeentie 68?0316',
'Sörnäinen(M)?Hämeentie?0314',
'Käenkuja?Hämeentie 44?0312',
'Haapaniemi?Hämeentie 26?0310',
'Hakaniemi?Siltasaarenkatu?0308',
'Kaisaniemenpuisto?Kaisaniemenkatu?0306',
'Kaisaniemenkatu?Kaisaniemenkatu?0304',
'Rautatieasema?Kaivokatu?0302',
'Ylioppilastalo?Mannerheimintie?0702',
'Erottaja?Bulevardi 3?0802',
'Fredrikinkatu?Bulevardi 9?0804',
'Aleksant. teatteri?Bulevardi 25?0806',
'Hietalahdentori?Hietalahdenkatu?0808',
'Hietalahdenkatu?Hietalahdenkatu?0810',
'Kalevankatu?Kalevankatu?0812',
'Hietalahti?Ruoholahdenranta?0811'];

$stops['1006T.1']=[
'Länsiterminaali?Tyynenmerenkatu?0200',
'Bunkkeri?Tyynenmerenkatu?0299',
'Huutokonttori?Tyynenmerenkatu?0297',
'Hietalahti?Ruoholahdenranta?0811',
'Köydenpunojankatu?Hietalahdenranta 15?0809',
'Hietalahdentori?Bulevardi 42?0807',
'Aleksant. teatteri?Bulevardi 30?0805',
'Fredrikinkatu?Bulevardi 18-20?0803',
'Erottaja?Bulevardi 6?0801',
'Ylioppilastalo?Mannerheimintie 1?0701',
'Rautatieasema?Kaivokatu?0301',
'Kaisaniemenkatu?Kaisaniemenkatu 7?0303',
'Kaisaniemenpuisto?Kaisaniemenkatu?0305',
'Hakaniemi?Siltasaarenkatu?0307',
'Haapaniemi?Hämeentie?0309',
'Käenkuja?Hämeentie 15?0311',
'Sörnäinen(M)?Hämeentie 33?0313',
'Lautatarhankatu?Hämeentie?0315',
'Hauhon puisto?Hämeentie 63?0317',
'Vallilan varikko?Hämeentie 85?0319',
'Paavalin kirkko?Hämeentie?0321',
'Kyläsaarenkatu?Hämeentie?0323',
'Toukoniitty?Hämeentie?0325',
'Arabiankatu?Arabiankatu?0335',
'Arabianranta?Arabianranta?0327'];

$stops['1006T.2']=[
'Arabianranta?Arabianranta?0327',
'Arabiankatu?Arabiankatu 2?0336',
'Toukoniitty?Hämeentie 122?0328',
'Kyläsaarenkatu?Hämeentie?0326',
'Paavalin kirkko?Hämeentie?0330',
'Vallilan varikko?Hämeentie 94?0320',
'Hauhon puisto?Hämeentie?0318',
'Lautatarhankatu?Hämeentie 68?0316',
'Sörnäinen(M)?Hämeentie?0314',
'Käenkuja?Hämeentie 44?0312',
'Haapaniemi?Hämeentie 26?0310',
'Hakaniemi?Siltasaarenkatu?0308',
'Kaisaniemenpuisto?Kaisaniemenkatu?0306',
'Kaisaniemenkatu?Kaisaniemenkatu?0304',
'Rautatieasema?Kaivokatu?0302',
'Ylioppilastalo?Mannerheimintie?0702',
'Erottaja?Bulevardi 3?0802',
'Fredrikinkatu?Bulevardi 9?0804',
'Aleksant. teatteri?Bulevardi 25?0806',
'Hietalahdentori?Hietalahdenkatu?0808',
'Hietalahdenkatu?Hietalahdenkatu?0810',
'Kalevankatu?Kalevankatu?0812',
'Huutokonttori?Tyynenmerenkatu?0296',
'Bunkkeri?Tyynenmerenkatu?0298',
'Länsiterminaali?Tyynenmerenkatu?0200'];

$stops['1007A.1']=[
'Hallituskatu?Snellmaninkatu 4?0450',
'Senaatintori?Aleksanterinkatu?0406',
'Aleksanterinkatu?Aleksanterinkatu 13?0404',
'Ylioppilastalo?Aleksanterinkatu?0402',
'Lasipalatsi?Mannerheimintie 9?0101',
'Kansallismuseo?Mannerheimintie?0103',
'Hesperian puisto?Mannerheimintie?0105',
'Ooppera?Mannerheimintie?0107',
'Töölön halli?Mannerheimintie 29?0109',
'Kansaneläkelaitos?Nordenskiöldinkatu 9?0641',
'Auroran sairaala?Nordenskiöldinkatu?0640',
'Eläintarha?Veturitie?0601',
'Palkkatilanportti?Pasilanraitio 1?0603',
'Maistraatintori?Pasilanraitio?0605',
'Kyllikinportti?Kyllikinportti 2?0607',
'Pasilan asema?Ratapihantie 7?0611'];

$stops['1007A.2']=[
'null?null?null'];

$stops['1007B.1']=[
'Hallituskatu?Snellmaninkatu 3?0451',
'Kansallisarkisto?Snellmaninkatu 13?0453',
'Snellmaninkatu?Liisankatu 25?0455',
'Hakaniemi?Siltasaarenkatu?0307',
'Haapaniemi?Hämeentie?0309',
'Käenkuja?Hämeentie 15?0311',
'Sörnäinen(M)?Hämeentie 33?0313',
'Lautatarhankatu?Mäkelänkatu 3?0331',
'Vallilan kirjasto?Mäkelänkatu 13?0333',
'Hattulantie?Mäkelänkatu 31?0265',
'Rautalammintie?Mäkelänkatu 37?0267',
'Mäkelänrinne?Mäkelänkatu 45?0269',
'Uintikeskus?Mäkelänkatu?0271',
'Radanrakentajantie?Radanrakentajantie?0618',
'Kellosilta?Ratamestarinkatu 9?0616',
'Messukeskus?Rautatieläisenkatu 5?0614',
'Pasilan asema?Ratapihantie?0620'];

$stops['1007B.2']=[
'null?null?null'];

$stops['1008.1']=[
'Saukonpaasi?Länsisatamankatu?0295',
'Crusellinsilta?Länsisatamankatu?0293',
'Länsisatamankatu?Länsisatamankatu?0291',
'Ruoholahti(M)?Itämerenkatu 19?0213',
'Itämerenkatu?Itämerenkatu 4?0215',
'Marian sairaala?Mechelininkatu?0217',
'Perhonkatu?Mechelininkatu?0219',
'Caloniuksenkatu?Mechelininkatu 21?0221',
'Apollonkatu?Runeberginkatu?0207',
'Töölöntori, Laituri 22?Runeberginkatu 55?0209',
'Ooppera?Helsinginkatu?0241',
'Kaupunginpuutarha?Helsinginkatu?0243',
'Diakoniapuisto?Helsinginkatu?0245',
'Urheilutalo?Helsinginkatu 28?0247',
'Helsinginkatu?Helsinginkatu 16?0249',
'Sörnäinen(M)?Hämeentie 33?0313',
'Lautatarhankatu?Hämeentie?0315',
'Hauhon puisto?Hämeentie 63?0317',
'Vallilan varikko?Hämeentie 85?0319',
'Paavalin kirkko?Hämeentie?0321',
'Kyläsaarenkatu?Hämeentie?0323',
'Toukoniitty?Hämeentie?0325',
'Arabiankatu?Arabiankatu?0335',
'Arabianranta?Arabianranta?0327'];

$stops['1008.2']=[
'Arabianranta?Arabianranta?0327',
'Arabiankatu?Arabiankatu 2?0336',
'Toukoniitty?Hämeentie 122?0328',
'Kyläsaarenkatu?Hämeentie?0326',
'Paavalin kirkko?Hämeentie?0330',
'Vallilan varikko?Hämeentie 94?0320',
'Hauhon puisto?Hämeentie?0318',
'Lautatarhankatu?Hämeentie 68?0316',
'Sörnäinen(M)?Hämeentie?0314',
'Helsinginkatu?Helsinginkatu?0250',
'Urheilutalo?Helsinginkatu 25?0248',
'Diakoniapuisto?Helsinginkatu?0246',
'Kaupunginpuutarha?Helsinginkatu?0244',
'Ooppera?Helsinginkatu?0242',
'Töölöntori, Laituri 21?Runeberginkatu 46?0210',
'Apollonkatu?Runeberginkatu 32?0208',
'Caloniuksenkatu?Mechelininkatu 20?0222',
'Perhonkatu?Mechelininkatu 4?0220',
'Marian sairaala?Mechelininkatu?0218',
'Itämerenkatu?Itämerenkatu 5?0216',
'Ruoholahti(M)?Itämerenkatu 14?0214',
'Länsisatamankatu?Länsisatamankatu?0290',
'Crusellinsilta?Länsisatamankatu?0292',
'Saukonpaasi?Länsisatamankatu?0294'];

$stops['1009.1']=[
'Länsiterminaali?Tyynenmerenkatu?0200',
'Bunkkeri?Tyynenmerenkatu?0299',
'Huutokonttori?Tyynenmerenkatu?0297',
'Länsilinkki?Ruoholahdenranta?0820',
'Ruoholahden villat?Ruoholahdenkatu?0822',
'Kampintori, Laituri 80?Malminrinne?0824',
'Simonkatu?Simonkatu?0232',
'Rautatieasema?Kaivokatu?0301',
'Kaisaniemenkatu?Kaisaniemenkatu 7?0303',
'Kaisaniemenpuisto?Kaisaniemenkatu?0305',
'Hakaniemi?Siltasaarenkatu?0251',
'Kallion virastotalo?Siltasaarenkatu 11?0253',
'Karhupuisto?Viides linja?0255',
'Kaarlenkatu?Kaarlenkatu 11?0257',
'Helsinginkatu?Fleminginkatu?0651',
'Fleminginkatu?Aleksis Kiven Katu?0653',
'Sturenkatu?Aleksis Kiven Katu?0655',
'Kotkankatu?Aleksis Kiven Katu?0657',
'Pasilan konepaja?Traverssikuja?0659',
'Jämsänkatu?Kumpulantie?0661',
'Asemapäällikönkatu?Ratamestarinkatu?0663',
'Kellosilta?Ratamestarinkatu 9?0616',
'Messukeskus?Rautatieläisenkatu 5?0614',
'Pasilan asema?Ratapihantie?0612'];

$stops['1009.2']=[
'Pasilan asema?Ratapihantie?0612',
'Asemapäällikönkatu?Ratamestarinkatu?0662',
'Jämsänkatu?Kumpulantie?0660',
'Pasilan konepaja?Traverssikuja?0658',
'Kotkankatu?Aleksis Kiven Katu?0656',
'Sturenkatu?Aleksis Kiven Katu?0654',
'Fleminginkatu?Fleminginkatu?0652',
'Helsinginkatu?Helsinginkatu?0250',
'Kaarlenkatu?Kaarlenkatu?0258',
'Karhupuisto?Viides linja?0256',
'Kallion virastotalo?Porthaninkatu 2?0254',
'Hakaniemi?Siltasaarenkatu?0252',
'Kaisaniemenpuisto?Kaisaniemenkatu?0306',
'Kaisaniemenkatu?Kaisaniemenkatu?0304',
'Rautatieasema?Kaivokatu?0302',
'Simonkatu?Simonkatu?0231',
'Kampintori, Laituri 79?Malminrinne?0825',
'Ruoholahden villat?Ruoholahdenkatu?0823',
'Länsilinkki?Ruoholahdenranta?0821',
'Huutokonttori?Tyynenmerenkatu?0296',
'Bunkkeri?Tyynenmerenkatu?0298',
'Länsiterminaali?Tyynenmerenkatu?0200'];

$stops['1010.1']=[
'Tarkk´ampujankatu, Laituri 22?Tarkk´ampujankatu 2?0708',
'Kirurgi, Laituri 21?Kasarmikatu 11?0707',
'Johanneksenkirkko?Yrjönkatu?0705',
'Kolmikulma?Erottajankatu 11?0703',
'Ylioppilastalo?Mannerheimintie 1?0701',
'Lasipalatsi?Mannerheimintie 9?0101',
'Kansallismuseo?Mannerheimintie?0103',
'Hesperian puisto?Mannerheimintie?0105',
'Ooppera?Mannerheimintie?0107',
'Töölön halli?Mannerheimintie 29?0109',
'Kansaneläkelaitos?Mannerheimintie 47?0111',
'Töölön tulli, Laituri 21?Mannerheimintie 75?0133',
'Jalavatie?Mannerheimintie 85?0135',
'Kuusitie?Mannerheimintie?0137',
'Tilkka?Mannerheimintie?0139',
'Ruskeasuo?Korppaanmäentie 1?0141',
'Kytösuontie?Korppaanmäentie 7?0143',
'Haapalahdenkatu?Korppaanmäentie?0145',
'Korppaanmäki?Korppaanmäentie?0147'];

$stops['1010.2']=[
'Korppaanmäki?Korppaanmäentie?0148',
'Haapalahdenkatu?Korppaanmäentie?0146',
'Kytösuontie?Korppaanmäentie 10?0144',
'Ruskeasuo?Korppaanmäentie 4?0142',
'Tilkka?Mannerheimintie?0140',
'Kuusitie?Mannerheimintie 150?0138',
'Jalavatie?Mannerheimintie 132?0136',
'Töölön tulli, Laituri 22?Mannerheimintie 116?0134',
'Kansaneläkelaitos?Mannerheimintie 94?0112',
'Töölön halli?Mannerheimintie 76?0110',
'Ooppera?Mannerheimintie 58?0108',
'Hesperian puisto?Mannerheimintie 42?0106',
'Kansallismuseo?Mannerheimintie?0104',
'Lasipalatsi?Mannerheimintie?0102',
'Ylioppilastalo?Mannerheimintie?0702',
'Kolmikulma?Erottajankatu?0704',
'Johanneksenkirkko?Yrjönkatu?0706',
'Tarkk´ampujankatu, Laituri 22?Tarkk ampujankatu 2?0708'];

/**
 * For-loop goes through all the data above (stored in _lista_-array). Then it is stored again in _stoplist_-array and
 * inserted into the collection _stops_
 */
for($i=0; $i<count($lines); $i++) {
    for($direction=1; $direction<=2; $direction++){
       for($k = 0; $k < count($stops[$lines[$i].'.'.$direction]); $k++){
            $lista = explode("?",$stops[$lines[$i].'.'.$direction][$k]);
            $stoplist = array(
                "_id" => $lines[$i].$direction.$k,     
                "line" => $lines[$i], 
                "dir" => (string)$direction,
                "stopid" => $lista[2],
                "stopname" => $lista[0],
                "stopadd" => $lista[1]
            );      
        $collection->insert($stoplist);
        } 
    }
}

?>