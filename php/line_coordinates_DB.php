<?php
/**
* jaarittelu
*/


/**
 * @todo  Connection to mongoDB is made here. 
 * 
 * An array named _lines_ is created defining the tram line id's. This is used in the for-loop below.
 * @return joasfdofjsad
 */
 
$lines = ['1001', '1001A', '1002', '1003', '1004', '1004T', '1006', '1006T', '1007A', '1007B', '1008', '1009', '1010'];
$connection = new MongoClient();
$collection = $connection->traminfo->linecoordinates;
$collection->drop();

/**
 * This for-loop goes through the data for all the lines, parses the data and inserts it to _linecoordinates_ collection
 */
for($i=0; $i<count($lines); $i++) {
  for($direction=1; $direction<=2; $direction++){
    $k = 0;
    $lineinfo_url = 'http://83.145.232.209:10001/?type=routewgs&line='.$lines[$i].'&direction='.$direction;
    $lineinfo = file_get_contents($lineinfo_url);
    $lineinfo_array = explode(';', $lineinfo);
    $lineinfo_array = implode(':', $lineinfo_array);
    $lineinfo_array = explode(':', $lineinfo_array);
    array_pop($lineinfo_array);
    $lineinfo_array = str_replace(',', '.', $lineinfo_array);
    
    for($j=0; $j<count($lineinfo_array); $j+=2){
      
       $document= array( 
        "_id" => $lines[$i].$direction.$k,
        "line" => $lines[$i], 
        "dir" => (string)$direction, 
        "lat" => (float)$lineinfo_array[$j],
        "lng" => (float)$lineinfo_array[$j+1]
      );
      $collection->insert($document);
      $k++;
    }
  }
}

?>
