<?php
/**
 * Connection to mongoDB is made here. 
 * An array named _lines_ is created defining the tram line id's. This is used in the for-loop below.
 * 
 * Also coordinates for a stop called bunkkeri is added here because it isn't included in some lines.
 */
 
$lines = ['1001', '1001A', '1002', '1003', '1004', '1004T', '1006', '1006T', '1007A', '1007B', '1008', '1009', '1010'];
$bunkkeriLat = '60.155732';
$bunkkeriLng = '24.921391';
$connection = new MongoClient();
$collection = $connection->traminfo->stopcoordinates;
$collection->drop();


/**
 * This for-loop goes through the data for all the stops, parses the data and inserts it to _stopcoordinates_ collection
 * This for-loop is a bit more complex than others because bunkkeri is added at the right place of the database.
 */
for($i=0; $i<count($lines); $i++) {
  for($direction=1; $direction<=2; $direction++){
    $n = 0;
    $stopinfo_url = 'http://83.145.232.209:10001/?type=stoplocations&line='.$lines[$i].'&direction='.$direction;
    $stopinfo = file_get_contents($stopinfo_url);
    $stopinfo_array = preg_split('/\s+/', $stopinfo);
    $stopinfo_lat = array();
    $stopinfo_lng = array();
 
    for($k=1; $k<=count($stopinfo_array); $k++){
        $firstpos = strpos($stopinfo_array[$k], ';');
        $lastpos = strrpos($stopinfo_array[$k], ';');
        $lat = substr($stopinfo_array[$k], $firstpos + 1, $lastpos - $firstpos - 1);
        $lng = substr($stopinfo_array[$k], $lastpos + 1);
        array_push($stopinfo_lat, $lat);
        array_push($stopinfo_lng, $lng);
    }
    
    array_pop($stopinfo_lat);
    array_pop($stopinfo_lng);
    array_pop($stopinfo_lat);
    array_pop($stopinfo_lng);
    
    if(($lines[$i] === '1009' || $lines[$i] === '1006T') && $direction === 1) {
      $m = 0;
      for($j=0; $j<count($stopinfo_lat) + 1; $j++){
        if($j === 1) {
          $document= array( 
          "_id" => $lines[$i].$direction.$n,
          "line" => $lines[$i], 
          "dir" => (string)$direction, 
          "lat" => $bunkkeriLat,
          "lng" => $bunkkeriLng
          );
          $collection->insert($document);
          $n++;
        } else {
         $document= array( 
          "_id" => $lines[$i].$direction.$n,
          "line" => $lines[$i], 
          "dir" => (string)$direction, 
          "lat" => $stopinfo_lat[$m],
          "lng" => $stopinfo_lng[$m]
        );
        $collection->insert($document);
        $n++;
        $m++;
        }
      }
    } else {
      for($j=0; $j<count($stopinfo_lat); $j++){
        $document= array( 
          "_id" => $lines[$i].$direction.$n,
          "line" => $lines[$i], 
          "dir" => (string)$direction, 
          "lat" => $stopinfo_lat[$j],
          "lng" => $stopinfo_lng[$j]
        );
        $collection->insert($document);
        $n++;
      }
    }   
  }
}

?>
