"use strict";
var browserSupportFlag = Boolean();
var myLatLng;
var map;
var coordinates;
var Path;
var paths = [];
var markers = [];
var tramMarkers = [];
var line = '';
var direction = '';
var timeout;

/**
 * This function checks if geolocation works.
 */
function initMap() {
    if (navigator.geolocation) {
        browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function(position) {
            myLatLng = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: myLatLng
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Sijaintisi'
            });
        }, function() {
            handleNoGeolocation(browserSupportFlag);
        });
    }
    // Browser doesn't support Geolocation
    else {
        browserSupportFlag = false;
        handleNoGeolocation(browserSupportFlag);
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag == true) {
            alert("Geolocation service failed.");
        } else {
            alert("Your browser doesn't support geolocation.");
        }
        myLatLng = {
            lat: 60.168027,
            lng: 24.937656
        };
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Sijaintisi'
        });
    }
}

window.onload = function() {
    clearTimeout(timeout);
    var hash = window.location.hash;
    removeLines();
    removeStopMarkers();
    removeTramMarkers();
    if (hash.length > 1) {
        line = hash.substring(1, hash.indexOf('&'));
        direction = hash.substring(hash.indexOf('&') + 1);
        drawLines(line, direction);
    }
};

$(window).on('hashchange', function() {
    clearTimeout(timeout);
    var hash = window.location.hash;
    removeLines();
    removeStopMarkers();
    removeTramMarkers();
    if (hash.length > 1) {
        line = hash.substring(1, hash.indexOf('&'));
        direction = hash.substring(hash.indexOf('&') + 1);
        drawLines(line, direction);
    }
});

/**
 * This function removes drawn tramlines from map.
 */
function removeLines() {
    if (paths) {
        var i;
        for (i = 0; i < paths.length; i += 1) {
            paths[i].setMap(null);
        }
        paths.length = 0;
    }
}

/**
 * This function removes drawn stop markers from map.
 */
function removeStopMarkers() {
    if (markers) {
        var i;
        for (i = 0; i < markers.length; i += 1) {
            markers[i].setMap(null);
        }
        markers.length = 0;
    }
}

/**
 * This function removes drawn tram markers from map.
 */
function removeTramMarkers() {
    if (tramMarkers) {
        var i;
        for (i = 0; i < tramMarkers.length; i += 1) {
            tramMarkers[i].setMap(null);
        }
        tramMarkers.length = 0;
    }
}

/**
 * This function draws tramlines on map.
 */
function drawLines(line, direction) {
    $.getJSON('https://ryhma10-mentori.c9users.io/api/line/' + line + '/direction/' + direction, function(data) {
        var k = 0;
        var latitudes = [];
        var longitudes = [];
        while (data[line + direction + k] != undefined) {
            latitudes.push(data[line + direction + k]['lat']);
            longitudes.push(data[line + direction + k]['lng']);
            k += 1;
        }
        var coordinates = [];
        var i;
        for (i = 0; i < latitudes.length; i += 1) {
            var cords = {
                lat: latitudes[i],
                lng: longitudes[i]
            };
            coordinates.push(cords);
        }
        Path = new google.maps.Polyline({
            path: coordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 3
        });
        paths.push(Path);
        Path.setMap(map);
    });
    drawStops(line, direction);
}

/**
 * This function draws tram stops on map.
 */
function drawStops(line, direction) {
    $.getJSON('https://ryhma10-mentori.c9users.io/api/stoplocation/' + line + '/direction/' + direction, function(data) {
        var k = 0;
        var latitudes = [];
        var longitudes = [];
        while (data[line + direction + k] != undefined) {
            latitudes.push(data[line + direction + k]['lat']);
            longitudes.push(data[line + direction + k]['lng']);
            k += 1;
        }
        var infowindow = new google.maps.InfoWindow();
        $.getJSON('https://ryhma10-mentori.c9users.io/api/stopname/' + line + '/direction/' + direction, function(stopJSON) {
            var j = 0;
            var stopNames = [];
            var stopAddresses = [];
            var stopIDs = [];
            while (stopJSON[line + direction + j] != undefined) {
                stopNames.push(stopJSON[line + direction + j]['stopname']);
                stopAddresses.push(stopJSON[line + direction + j]['stopadd']);
                stopIDs.push(stopJSON[line + direction + j]['stopid']);
                j += 1;
            }
            var markerImage = new google.maps.MarkerImage('stop.png',
                new google.maps.Size(30, 30),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 13));
            var i, pos, marker;
            for (i = 0; i < latitudes.length; i += 1) {
                pos = new google.maps.LatLng(latitudes[i], longitudes[i]);
                marker = new google.maps.Marker({
                    position: pos,
                    icon: markerImage,
                    title: stopNames[i]
                });
                var content = '<font color = black><b>' + stopNames[i] + ' ' + stopAddresses[i] + ' ' + stopIDs[i] + '</b></font>';
                google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));
                markers.push(marker);
                marker.setMap(map);
            }
        });
    });
    drawTrams(line, direction);
}

/**
 * This function draws tram stops on map.
 */
function drawTrams(line, direction) {
    var markerImage = new google.maps.MarkerImage('tram.png',
        new google.maps.Size(24, 24),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 24));
    $.getJSON('https://ryhma10-mentori.c9users.io/api/tram/' + line + '/direction/' + direction, function(data) {
        coordinates = data;
        var i;
        for (i = 0; i < coordinates.length; i += 1) {
            var str = coordinates[i],
                delimiter = ';',
                tokens = str.split(delimiter).slice(3, 4),
                tokens2 = str.split(delimiter).slice(2, 3),
                result = tokens.join(delimiter),
                result2 = tokens2.join(delimiter);
            var pos = new google.maps.LatLng(result, result2);
            var marker = new google.maps.Marker({
                position: pos,
                icon: markerImage,
                zIndex: google.maps.Marker.MAX_ZINDEX + 1,
                title: "Spora"
            });
            tramMarkers.push(marker);
            marker.setMap(map);
        }
    });
    timeout = setTimeout("drawTrams(line, direction)", 5000);
    removeTramMarkers();
}