<?php
/**
 * This php file is used for REST-api
 */ 



/**
 * URI parser helper functions getResource(), getParameters() and getMethod()
 */
    function getResource() {
        # returns numerically indexed array of URI parts
        $resource_string = $_SERVER['REQUEST_URI'];
        if (strstr($resource_string, '?')) {
            $resource_string = substr($resource_string, 0, strpos($resource_string, '?'));
        }
        $resource = array();
        $resource = explode('/', $resource_string);
        array_shift($resource);   
        return $resource;
    }

    function getParameters() {
        # returns an associative array containing the parameters
        $resource = $_SERVER['REQUEST_URI'];
        $param_string = "";
        $param_array = array();
        if (strstr($resource, '?')) {
            # URI has parameters
            $param_string = substr($resource, strpos($resource, '?')+1);
            $parameters = explode('&', $param_string);                      
            foreach ($parameters as $single_parameter) {
                $param_name = substr($single_parameter, 0, strpos($single_parameter, '='));
                $param_value = substr($single_parameter, strpos($single_parameter, '=')+1);
                $param_array[$param_name] = $param_value;
            }
        }
        return $param_array;
    }

    function getMethod() {
        # returns a string containing the HTTP method
        $method = $_SERVER['REQUEST_METHOD'];
        return $method;
    }
 

/**
 * Handlers
 */
	function route($line, $direction) {
	    $connection = new MongoClient();
        $collection = $connection->traminfo->linecoordinates;
        $query = array('line' => $line, 'dir' => $direction);
        $cursor = $collection->find($query);
        $array = iterator_to_array($cursor);
        echo json_encode($array);
	}
        
    function stopLocations($line, $direction) {
	    $connection = new MongoClient();
        $collection = $connection->traminfo->stopcoordinates;
        $query = array('line' => $line, 'dir' => $direction);
        $cursor = $collection->find($query);
        $array = iterator_to_array($cursor);
        echo json_encode($array);
    }
	
	function stopNames($line, $direction) {
	    $connection = new MongoClient();
        $collection = $connection->traminfo->stops;
        $query = array('line' => $line, 'dir' => $direction);
        $cursor = $collection->find($query);
        $array = iterator_to_array($cursor);
        echo json_encode($array);
	}
	
	function tramLocations($line, $direction) {
        $lineinfoAll = 'http://83.145.232.209:10001/?type=vehicles&vehicletype=1&lng1=0&lng2=99&lat1=0&lat2=99&online=1';
        $lineinfoAll_str = file_get_contents($lineinfoAll);
        
        $lineinfoAll_array = preg_split('/\s+/', $lineinfoAll_str);
        $lineinfo = array();
        for($i=1; $i<count($lineinfoAll_array); $i++){
            if(strpos($lineinfoAll_array[$i], ';'.$line.';') && strpos($lineinfoAll_array[$i], ';'.$direction.';')){
                array_push($lineinfo, $lineinfoAll_array[$i]);
            }
        }
        echo json_encode($lineinfo);
	}


/**
 * Routing
 */
	$resource = getResource();
    $request_method = getMethod();
    $parameters = getParameters();

    # Redirect to appropriate handlers.
	if ($resource[0]=="api") {
    	if ($request_method=="GET" && $resource[1]=="line" && $resource[3]=="direction") {
        	route($resource[2], $resource[4]);
    	}
    	else if ($request_method=="GET" && $resource[1]=="stoplocation" && $resource[3]=="direction" ) {
    	    stopLocations($resource[2], $resource[4]);
    	}
    	else if ($request_method=="GET" && $resource[1]=="stopname" && $resource[3]=="direction" ) {
    	    stopNames($resource[2], $resource[4]);
    	}
    	else if ($request_method=="GET" && $resource[1]=="tram" && $resource[3]=="direction" ) {
    	    tramLocations($resource[2], $resource[4]);
    	}
		else {
			http_response_code(405); # Method not allowed
		}
	}

?>
